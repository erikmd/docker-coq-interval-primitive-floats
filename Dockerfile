ARG COQ_VERSION="8.15"
FROM coqorg/coq:${COQ_VERSION}-native-flambda

ARG MATHCOMP_VERSION="dev"
ARG INTERVAL_VERSION="dev"
ARG INTERVAL_COMMIT=""
ARG FLOCQ_VERSION=""

WORKDIR /home/coq

RUN ["/bin/bash", "--login", "-c", "set -x \
  && opam config list; opam repo list; opam list \
  && if [ -n \"${FLOCQ_VERSION}\" ]; then opam repository add --all-switches --set-default coq-extra-dev https://coq.inria.fr/opam/extra-dev; fi \
  && opam update -y \
  && opam pin add -n -y -k version coq-mathcomp-ssreflect ${MATHCOMP_VERSION} \
  && if [ -n \"${FLOCQ_VERSION}\" ]; then opam pin add -n -y -k version coq-flocq ${FLOCQ_VERSION}; fi \
  && if [ -n \"${INTERVAL_COMMIT}\" ]; then opam pin add -n -y -k git coq-interval.${INTERVAL_VERSION} \"git+https://gitlab.inria.fr/coqinterval/interval.git#${INTERVAL_COMMIT}\"; else opam pin add -n -y -k version coq-interval ${INTERVAL_VERSION}; fi \
  && ulimit -s \
  && ulimit -s 32768 \
  && ulimit -s \
  && opam install -y -v -j ${NJOBS} coq-interval"]

CMD ["/bin/bash"]
